/*
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created 11/25/2020
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CODICE_LENGTH 6
#define STRING_LENGTH 50
#define STATS_LENGTH 6
#define COMMAND_LENGTH 25
#define COMMANDS_NUMBER 8
#define MAX_EQUIPS 8

// Posizioni delle statistiche nel vettore, per comodità le salvo in alcune costanti.
#define HP_INDEX 0
#define MP_INDEX 1
#define ATK_INDEX 2
#define DEF_INDEX 3
#define MAG_INDEX 4
#define SPR_INDEX 5

// Struttura rappresentante un equipaggiamento.
typedef struct {
    char nome[STRING_LENGTH + 1];
    char tipo[STRING_LENGTH + 1];
    int stat[STATS_LENGTH];
} inv_t;

// Struttura rappresentante l'inventario.
typedef struct {
    inv_t *vettInv;
    int nInv;
    int maxInv;
} tabInv_t;

// Struttura rappresentante gli equipaggiamenti.
typedef struct {
    int inUso;
    inv_t **vettEq;
} tabEquip_t;

// Struttura rappresentante le statistiche.
typedef struct {
    int hp;
    int mp;
    int atk;
    int def;
    int mag;
    int spr;
} stat_t;

// Struttura rappresentante un personaggio.
typedef struct {
    char codice[CODICE_LENGTH + 1];
    char nome[STRING_LENGTH + 1];
    char classe[STRING_LENGTH + 1];
    tabEquip_t *equip;
    stat_t stat;
} pg_t;

// Struttura rappresentante un nodo nella lista.
struct nodoPg_t {
    pg_t personaggio;
    struct nodoPg_t *next;
};

// Struttura rappresentante una lista.
typedef struct {
    struct nodoPg_t *headPg;
    struct nodoPg_t *tailPg;
    int nPg;
} tabPg_t;

// Enum dei comandi disponibili.
typedef enum comando_e{
    carica_lista,
    carica_oggetti,
    aggiungi_personaggio,
    elimina_personaggio,
    aggiungi_equipaggiamento,
    rimuovi_equipaggiamento,
    stampa_statistiche,
    fine
};

// Definisco le funzioni.
void loadPlayers(tabPg_t *tab);
void loadObjects(tabInv_t *tab);
void addPlayer(tabPg_t *tab);
void removePlayer(tabPg_t *tab);
void addEquip(tabPg_t *tabPg, tabInv_t *tabInv);
void removeEquip(tabPg_t *tabPg);
void getStats(tabPg_t *tab);
struct nodoPg_t *newNode(pg_t player, struct nodoPg_t *next);
void insert(tabPg_t **tab, pg_t player);
struct nodoPg_t *delete(tabPg_t **tab, char code[CODICE_LENGTH + 1]);
struct nodoPg_t *search(tabPg_t *tab, char code[CODICE_LENGTH + 1]);
void printPlayer(pg_t player);
int leggiComando();

int main() {
    tabPg_t listaPersonaggi;
    tabInv_t listaOggetti;
    int continua = 1;

    // Imposto i parametri default.
    listaPersonaggi.nPg = 0;
    listaPersonaggi.headPg = NULL;
    listaPersonaggi.tailPg = NULL;
    listaOggetti.nInv = 0;

    // Menu utente.
    while (continua) {
        switch (leggiComando()) {
            case carica_lista:
                loadPlayers(&listaPersonaggi);
                break;
            case carica_oggetti:
                loadObjects(&listaOggetti);
                break;
            case aggiungi_personaggio:
                addPlayer(&listaPersonaggi);
                break;
            case elimina_personaggio:
                removePlayer(&listaPersonaggi);
                break;
            case aggiungi_equipaggiamento:
                addEquip(&listaPersonaggi, &listaOggetti);
                break;
            case rimuovi_equipaggiamento:
                removeEquip(&listaPersonaggi);
                break;
            case stampa_statistiche:
                getStats(&listaPersonaggi);
                break;
            default:
                continua = 0;
        }
    }
    return 0;
}

void loadPlayers(tabPg_t *tab)
{
    char fileName[STRING_LENGTH + 1];
    FILE *fp;
    pg_t player;

    printf("Inserire il nome del file: ");
    scanf("%s", fileName);

    fp = fopen(fileName, "r");

    // Gestione degli errori riguardanti il file.
    if (fp == NULL) {
        return;
    }

    for (int i = 0; !feof(fp); i++) {
        fscanf(
            fp,
            "%s %s %s %d %d %d %d %d %d",
            player.codice,
            player.nome,
            player.classe,
            &player.stat.hp,
            &player.stat.mp,
            &player.stat.atk,
            &player.stat.def,
            &player.stat.mag,
            &player.stat.spr
        );

        // Creo un equipaggiamento di default.
        player.equip = malloc(sizeof(tabEquip_t *));
        player.equip->inUso = -1;
        player.equip->vettEq = (inv_t **) malloc(MAX_EQUIPS * sizeof(inv_t *));

        // Inserisco il personaggio nella lista.
        insert(&tab, player);
        tab->nPg += 1;
    }
}

void addPlayer(tabPg_t *tab)
{
    pg_t tmp;

    printf("Inserire le informazioni (codice nome classe hp mp atk def mag spr): ");
    scanf(
            "%s %s %s %d %d %d %d %d %d",
            tmp.codice,
            tmp.nome,
            tmp.classe,
            &tmp.stat.hp,
            &tmp.stat.mp,
            &tmp.stat.atk,
            &tmp.stat.def,
            &tmp.stat.mag,
            &tmp.stat.spr
    );

    // Creo un equipaggiamento di default.
    tmp.equip->inUso = -1;

    // Inserisco il personaggio nella lista.
    insert(&tab, tmp);
    tab->nPg += 1;
}

void removePlayer(tabPg_t *tab)
{
    char code[CODICE_LENGTH + 1];
    struct nodoPg_t *tmp;

    printf("Inserire il codice dell'elemento da eliminare: ");
    scanf("%s", code);

    // Elimino il personaggio dalla lista.
    tmp = delete(&tab, code);

    if (tmp != NULL) {
        printf("Elemento eliminato!\n");
        // Posso deallocare la porzione di memoria che era occupata dall'elemento.
        free(*tmp->personaggio.equip->vettEq);
        free(tmp);
        return;
    }

    printf("Elemento non trovato!\n");
}

void loadObjects(tabInv_t *tab)
{
    char fileName[STRING_LENGTH + 1];
    FILE *fp;
    int objectsNumber;
    inv_t tmp;

    printf("Inserire il nome del file: ");
    scanf("%s", fileName);

    fp = fopen(fileName, "r");

    if (fp == NULL) {
        return;
    }

    fscanf(fp, "%d", &objectsNumber);

    // Creo un vettore di equipaggiamenti.
    tab->vettInv = (inv_t *) malloc(objectsNumber * sizeof(inv_t));

    for (int i = 0; i < objectsNumber; i++) {
        fscanf(
            fp,
            "%s %s %d %d %d %d %d %d",
            tmp.nome,
            tmp.tipo,
            &tmp.stat[HP_INDEX],
            &tmp.stat[MP_INDEX],
            &tmp.stat[ATK_INDEX],
            &tmp.stat[DEF_INDEX],
            &tmp.stat[MAG_INDEX],
            &tmp.stat[SPR_INDEX]
        );

        tab->vettInv[i] = tmp;
        tab->nInv = tab->nInv + 1;
    }
}

void addEquip(tabPg_t *tabPg, tabInv_t *tabInv)
{
    inv_t *equip = NULL;
    struct nodoPg_t *node;
    char equipName[STRING_LENGTH + 1], playerCode[CODICE_LENGTH + 1];

    printf("Inserire il nome dell'equipaggiamento da aggiungere: ");
    scanf("%s", equipName);

    // Ricerca lineare dell'equipaggiamento nell'inventario.
    for (int i = 0; i < tabInv->nInv; i++) {
        if (strcmp(tabInv->vettInv[i].nome, equipName) == 0) {
            equip = &tabInv->vettInv[i];
        }
    }

    if (equip == NULL) {
        printf("L'equipaggiamento cercato non esiste.");
        return;
    }

    printf("Inserire il codice del personaggio a cui aggiungere l'equipaggiamento: ");
    scanf("%s", playerCode);

    // Ricerca del personaggio dato il codice.
    node = search(tabPg, playerCode);

    if (node == NULL) {
        printf("Il personaggio cercato non esiste.");
        return;
    }

    if (node->personaggio.equip->inUso >= MAX_EQUIPS) {
        printf("Il personaggio dispone già di 8 equipaggiamenti, eliminarne alcuni e riprovare.\n");
        return;
    }

    node->personaggio.equip->vettEq[node->personaggio.equip->inUso] = equip;
    node->personaggio.equip->inUso += 1;
}

void removeEquip(tabPg_t *tabPg)
{
    char equipName[STRING_LENGTH + 1], playerCode[CODICE_LENGTH + 1];
    int index = -1;
    struct nodoPg_t *node;

    printf("Inserire il nome dell'equipaggiamento da rimuovere: ");
    scanf("%s", equipName);

    printf("Inserire il codice del personaggio a cui aggiungere l'equipaggiamento: ");
    scanf("%s", playerCode);

    node = search(tabPg, playerCode);

    if (node == NULL) {
        printf("Il personaggio cercato non esiste. \n");
        return;
    }

    // Ricerca dell'equipaggiamento tra quelli del personaggio.
    for (int i = 0; i < node->personaggio.equip->inUso; i++) {
        if (strcmp(node->personaggio.equip->vettEq[i]->nome, equipName) == 0) {
            index = i;
        }
    }

    if (index == -1) {
        printf("Il personaggio non contiene l'equipaggiamento cercato. \n");
        return;
    }

    node->personaggio.equip->inUso -= 1;
}

void getStats(tabPg_t *tab)
{
    char playerCode[CODICE_LENGTH + 1];
    pg_t tmp;
    struct nodoPg_t *node;

    printf("Inserire il codice del personaggio: ");
    scanf("%s", playerCode);

    node = search(tab, playerCode);

    if (node == NULL) {
        printf("Il personaggio cercato non esiste.\n");
        return;
    }

    // Creo un personaggio temporaneo per maneggiare le statistiche senza sovrascrivere quelle originali.
    tmp = node->personaggio;

    for (int i = 0; i < node->personaggio.equip->inUso; i++) {
        tmp.stat.hp += node->personaggio.equip->vettEq[i]->stat[HP_INDEX];
        tmp.stat.mp += node->personaggio.equip->vettEq[i]->stat[MP_INDEX];
        tmp.stat.atk += node->personaggio.equip->vettEq[i]->stat[ATK_INDEX];
        tmp.stat.def += node->personaggio.equip->vettEq[i]->stat[DEF_INDEX];
        tmp.stat.mag += node->personaggio.equip->vettEq[i]->stat[MAG_INDEX];
        tmp.stat.spr += node->personaggio.equip->vettEq[i]->stat[SPR_INDEX];
    }

    // Stampo le statistiche.
    printPlayer(tmp);
}

// Crea un nuovo nodo e lo ritorna.
struct nodoPg_t *newNode(pg_t player, struct nodoPg_t *next)
{
    struct nodoPg_t *node;
    node = (struct nodoPg_t *) malloc(sizeof(struct nodoPg_t));

    if (node == NULL) {
        return NULL;
    }

    node->personaggio = player;
    node->next = next;

    return node;
}

// Inserisce un nodo in testa alla lista.
void insert(tabPg_t **tab, pg_t player)
{
    if ((*tab)->headPg == NULL) {
        newNode(player, NULL);
        (*tab)->headPg = (*tab)->tailPg;
    }
    (*tab)->headPg = newNode(player, (*tab)->headPg);
}

// Elimina un nodo dalla lista.
struct nodoPg_t *delete(tabPg_t **tab, char code[CODICE_LENGTH + 1])
{
    struct nodoPg_t *tmp;
    struct nodoPg_t *before;

    if ((*tab)->headPg == NULL) {
        return NULL;
    }

    tmp = (*tab)->headPg;
    before = NULL;

    while (tmp != NULL && strcmp(code, tmp->personaggio.codice) != 0) {
        before = tmp;
        tmp = tmp->next;
    }

    if (tmp == NULL) {
        return NULL;
    }

    before->next = tmp->next;

    return tmp;
}

// Cerca un nodo dato il codice personaggio.
struct nodoPg_t *search(tabPg_t *tab, char code[CODICE_LENGTH + 1])
{
    struct nodoPg_t *tmp;

    for (tmp = tab->headPg; tmp != NULL && strcmp(tmp->personaggio.codice, code) != 0; tmp = tmp->next);

    return tmp;
}

// Stampa le statistiche del personaggio.
void printPlayer(pg_t player)
{
    printf("%s %s %s ", player.codice, player.nome, player.classe);

    // Se una statistica è < 0, stampa 0 come richiesto.
    printf(
        "hp: %d, mp: %d, atk: %d, def: %d, mag: %d, spr: %d ",
        player.stat.hp >= 0 ? player.stat.hp : 0,
        player.stat.mp >= 0 ? player.stat.mp : 0,
        player.stat.atk >= 0 ? player.stat.atk : 0,
        player.stat.def >= 0 ? player.stat.def : 0,
        player.stat.mag >= 0 ? player.stat.mag : 0,
        player.stat.spr >= 0 ? player.stat.spr : 0
    );

    printf("\n");
}

// Funzione menu.
int leggiComando()
{
    char comandi[COMMANDS_NUMBER][COMMAND_LENGTH + 1] = {
        "carica_lista",
        "carica_oggetti",
        "aggiungi_personaggio",
        "elimina_personaggio",
        "aggiungi_equipaggiamento",
        "rimuovi_equipaggiamento",
        "stampa_statistiche",
        "fine"
    };
    char comandoTmp[COMMAND_LENGTH + 1];
    int i = 0;

    printf("Inserire un comando (carica_lista, carica_oggetti, aggiungi_personaggio, elimina_personaggio, aggiungi_equipaggiamento, rimuovi_equipaggiamento, stampa_statistiche, fine): ");
    scanf("%s", comandoTmp);

    for (i = 0; i < COMMANDS_NUMBER && strcmp(comandoTmp, comandi[i]) != 0; i++);

    return i;
}
#include <stdio.h>
#include <stdlib.h>

#define MAX_STRING_LENGTH 50
#define NUM_VALUES 4
#define MAX_NAME_LENGTH 8

// Struct rappresentante una pietra.
typedef struct {
    int available;
    int value;
    int maxRip;
    int index;
} stone;

typedef enum tipoPietra {
    zaffiro,
    rubino,
    topazio,
    smeraldo
};

// Definisco le funzioni che utilizzerò.
int readFile(stone ***matrix);
int canInsert(stone current, stone *sol, int pos);
void disp_rip(int pos, stone *val, stone *sol, int n, int k, stone *bestSol, int *bestSolValue, int *found, int *solCount, int zNum, int sNum, int solValue);
int getValue(stone *sol, int dim);
void getCollana(stone val[NUM_VALUES]);

int main() {
    int nTot, sum;
    stone **matrix;

    // Vettore statico per memorizzare i nomi delle pietre.
    char names[4][MAX_NAME_LENGTH + 1] = {
            "zaffiro",
            "rubino",
            "topazio",
            "smeraldo"
    };

    nTot = readFile(&matrix);

    for (int i = 0; i < nTot; i++) {
        sum = 0;
        printf("==TEST #%d\n", i + 1);

        for (int j = 0; j < NUM_VALUES; j++) {
            sum += matrix[i][j].available;
            printf("%s: %d [%d] ", names[j], matrix[i][j].available, matrix[i][j].value);
        }
        printf("totale: %d ", sum);
        printf("{max_rip = %d}", matrix[i][0].maxRip);
        printf("\n");
        getCollana(matrix[i]);
    }

    // Libero ogni riga della matrice.
    for (int i = 0; i < nTot; i++) {
        free(matrix[i]);
    }

    // Libero la matrice.
    free(matrix);
    return 0;
}

// Funzione per effettuare il pruning: controllo che il valore da inserire rispetti i vincoli dettati nella consegna.
int canInsert(stone current, stone *sol, int pos)
{
    int maxRip = current.maxRip;
    int start = pos - 1, repeat = 0;

    if (pos == 0) {
        return 1;
    }

    // Conto le ripetizioni consecutive.
    while (start >= 0 && sol[start].index == current.index) {
        repeat ++;

        if (repeat >= maxRip) {
            return 0;
        }

        start --;
    }

    // Controllo che la pietra possa essere inserita.
    switch (sol[pos - 1].index) {
        case zaffiro:
        case topazio:
            return (current.index == zaffiro || current.index == rubino);
        case smeraldo:
        case rubino:
            return (current.index == smeraldo || current.index == topazio);
        default:
            return 0;
    }
}

// Leggo i valori da file e li salvo nella matrice, dopo averla allocata DINAMICAMENTE.
int readFile(stone ***matrix)
{
    FILE *fp;
    char fileName[MAX_STRING_LENGTH + 1];
    int nTot, maxRip;
    stone z, r, t, s;

    printf("Inserire il nome del file (max 50 caratteri): ");
    scanf("%s", fileName);

    fp = fopen(fileName, "r");

    if (fp == NULL) {
        return -1;
    }

    fscanf(fp, "%d", &nTot);

    *matrix = (stone **) malloc(nTot * sizeof(stone *));

    for (int i = 0; i < nTot; i++) {
        (*matrix)[i] = (stone *) malloc(NUM_VALUES * sizeof(stone));

        fscanf(
            fp,
            "%d %d %d %d %d %d %d %d %d",
            &z.available,
            &r.available,
            &t.available,
            &s.available,
            &z.value,
            &r.value,
            &t.value,
            &s.value,
            &maxRip
        );

        z.maxRip = r.maxRip = t.maxRip = s.maxRip = maxRip;

        z.index = zaffiro;
        r.index = rubino;
        t.index = topazio;
        s.index = smeraldo;

        (*matrix)[i][zaffiro] = z;
        (*matrix)[i][rubino] = r;
        (*matrix)[i][topazio] = t;
        (*matrix)[i][smeraldo] = s;
    }

    return nTot;
}

void disp_rip(int pos, stone *val, stone *sol, int n, int k, stone *bestSol, int *bestSolValue, int *found, int *solCount, int zNum, int sNum, int solValue)
{
    if (pos >= k) {
        // Controllo che il valore sia un nuovo massimo e che la soluzione rispetti l'ultima condizione richiesta.
        if (solValue > *bestSolValue && zNum <= sNum) {
            *found = 1;
            *bestSolValue = solValue;
            *solCount = pos;

            // Copio la soluzione.
            for (int i = 0; i <= pos; i++) {
                bestSol[i] = sol[i];
            }
        }

        return;
    }

    // Per ogni tipo di pietra.
    for (int i = 0; i < n; i++) {
        // Se ci sono ancora pietre di quel tipo.
        if (val[i].available > 0) {
            // Pruning.
            if (canInsert(val[i], sol, pos) && (zNum - sNum) <= val[smeraldo].available) {
                val[i].available --;

                // Incremento il contatore sugli zaffiri.
                if (val[i].index == zaffiro) {
                    zNum ++;
                }

                // incremento il contatore sugli smeraldi.
                if (val[i].index == smeraldo) {
                    sNum ++;
                }

                sol[pos] = val[i];
                solValue += val[i].value;

                // Chiamata ricorsiva.
                disp_rip(pos + 1, val, sol, n, k, bestSol, bestSolValue, found, solCount, zNum, sNum, solValue);

                // Backtracking.
                solValue -= val[i].value;

                if (val[i].index == zaffiro) {
                    zNum --;
                }

                if (val[i].index == smeraldo) {
                    sNum --;
                }

                val[i].available ++;
            }
        }
    }
}

// Ritorna il valore della soluzione.
int getValue(stone *sol, int dim)
{
    int sum = 0;

    for (int i = 0; i < dim; i++) {
        sum += sol[i].value;
    }

    return sum;
}

// Funzione wrapper.
void getCollana(stone val[NUM_VALUES])
{
    stone *sol, *bestSol, *lastSol;
    int bestSolValue = 0, sum = 0, found, length = 0, solCount = 0;

    // Vettore statico per memorizzare i nomi delle pietre.
    char names[4][MAX_NAME_LENGTH + 1] = {
            "zaffiro",
            "rubino",
            "topazio",
            "smeraldo"
    };

    for (int i = 0; i < NUM_VALUES; i++) {
        sum += val[i].available;
    }

    sol = (stone *) malloc(sum * sizeof(stone));
    bestSol = (stone *) malloc(sum * sizeof(stone));
    lastSol = (stone *) malloc(sum * sizeof(stone));

    int first = 0, last = sum, middle;

    // Ricerca binaria della lunghezza.
    while (first <= last) {
        middle = (first + last) / 2;
        found = 0;

        disp_rip(0, val, sol, NUM_VALUES, middle, bestSol, &bestSolValue, &found, &solCount, 0, 0, 0);

        if (found) {
            for (int i = 0; i < middle; i++) {
                lastSol[i] = bestSol[i];
            }

            first = middle + 1;
            length = middle;
        } else {
            last = middle - 1;
        }
    }

    printf("Soluzione ottima di valore %d usando %d gemma/e\n", getValue(bestSol, length), length);
    printf("Composizione collana: ");

    for (int i = 0; i < length; i++) {
        printf("%c", names[lastSol[i].index][0]);
    }

    printf("\n");

    // Rilascio i vettori.
    free(sol);
    free(bestSol);
    free(lastSol);
}
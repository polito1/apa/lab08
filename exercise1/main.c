/*
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created 11/30/2020
 */

#include <stdio.h>
#include <stdlib.h>

#define MAX_STRING_LENGTH 50
#define NUM_VALUES 4
#define MAX_NAME_LENGTH 8

typedef enum tipoPietra {
    zaffiro,
    rubino,
    topazio,
    smeraldo
};

int canInsert(int previous, int current);
void getCollana(int val[NUM_VALUES]);
void disp_rip(int pos, int *val, int *sol, int n, int k, int *found);
int readFile(int ***matrix);

int main() {
    int nTot, sum, **matrix;

    // Vettore statico per memorizzare i nomi delle pietre.
    char names[4][MAX_NAME_LENGTH + 1] = {
            "zaffiro",
            "rubino",
            "topazio",
            "smeraldo"
    };

    nTot = readFile(&matrix);

    for (int i = 0; i < nTot; i++) {
        sum = 0;
        printf("==TEST #%d\n", i + 1);

        for (int j = 0; j < NUM_VALUES; j++) {
            sum += matrix[i][j];
            printf("%s: %d ", names[j], matrix[i][j]);
        }
        printf("totale: %d", sum);
        printf("\n");
        getCollana(matrix[i]);
    }

    return 0;
}

// Funzione per effettuare il pruning: controllo che il valore da inserire rispetti i vincoli dettati nella consegna.
canInsert(int previous, int current)
{
    switch (previous) {
        case zaffiro:
            return (current == zaffiro || current == rubino);
        case smeraldo:
            return (current == smeraldo || current == topazio);
        case rubino:
            return (current == smeraldo || current == topazio);
        case topazio:
            return (current == zaffiro || current == rubino);
        default:
            return 0;
    }
}

// Leggo i valori da file e li salvo nella matrice, dopo averla allocata DINAMICAMENTE.
int readFile(int ***matrix)
{
    FILE *fp;
    char fileName[MAX_STRING_LENGTH + 1];
    int nTot, z, r, t, s;

    printf("Inserire il nome del file (max 50 caratteri): ");
    scanf("%s", fileName);

    fp = fopen(fileName, "r");

    if (fp == NULL) {
        return -1;
    }

    fscanf(fp, "%d", &nTot);

    *matrix = (int **) malloc(nTot * sizeof(int *));

    for (int i = 0; i < nTot; i++) {
        (*matrix)[i] = (int *) malloc(NUM_VALUES * sizeof(int));

        fscanf(fp, "%d %d %d %d", &z, &r, &t, &s);

        (*matrix)[i][zaffiro] = z;
        (*matrix)[i][rubino] = r;
        (*matrix)[i][topazio] = t;
        (*matrix)[i][smeraldo] = s;
    }

    return nTot;
}

// Ricerca dicotomica della lunghezza massima.
void getCollana(int val[NUM_VALUES])
{
    int *sol, bestSolLength = 0, sum = 0, first, last, middle, found;

    for (int i = 0; i < NUM_VALUES; i++) {
        sum += val[i];
    }

    sol = (int *) malloc(sum * sizeof(int));

    first = 0;
    last = sum;

    while (first <= last) {
        found = 0;
        middle = (first + last) / 2;

        disp_rip(0, val, sol, NUM_VALUES, middle, &found);

        if (found) {
            first = middle + 1;
            bestSolLength = middle;
        } else {
            last = middle - 1;
        }
    }

    printf("Collana massima di lunghezza: %d\n", bestSolLength);

    free(sol);
}

// Funzione che implementa le disposizioni con ripetizione con pruning.
void disp_rip(int pos, int *val, int *sol, int n, int k, int *found)
{
    int insert = 0;
    if (pos >= k) {
        *found = 1;
        return;
    }

    for (int i = 0; i < n && !(*found); i++) {
        if (val[i] > 0) {
            insert = pos == 0 ? 1 : canInsert(sol[pos - 1], i);
            if (insert) {
                sol[pos] = i;
                val[i] --;

                disp_rip(pos + 1, val, sol, n, k, found);
                val[i] ++;
            }
        }
    };
}